package br.com.sicredi.config;

import java.time.Duration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;

@Configuration
public class RedisConfiguration {
	
	@Bean
	public RedisCacheConfiguration getRedisCacheConfiguration() {
		return RedisCacheConfiguration.defaultCacheConfig()
							   		  .entryTtl(Duration.ofSeconds(60));
							   
		
	}

}
