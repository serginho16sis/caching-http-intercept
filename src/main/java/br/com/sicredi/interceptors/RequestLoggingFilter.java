package br.com.sicredi.interceptors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.AbstractRequestLoggingFilter;

public class RequestLoggingFilter extends AbstractRequestLoggingFilter {

	@Override
	protected void beforeRequest(HttpServletRequest request, String message) {
		RequestContext.setRequest(message);
	}

	@Override
	protected void afterRequest(HttpServletRequest request, String message) {
	}

}
