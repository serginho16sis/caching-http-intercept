package br.com.sicredi.contract.controller;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import java.net.URI;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.sicredi.contract.model.ProdutoIn;
import br.com.sicredi.documents.Produto;
import br.com.sicredi.service.ProdutoService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value="/api/produtos", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@AllArgsConstructor
public class ProdutoController {
	
	private ProdutoService produtoService;
	
	@ResponseStatus(OK)
	@GetMapping
	public List<Produto> listar(){
		return produtoService.buscarTodos();
	}
	
	@ResponseStatus(OK)
	@GetMapping("/{id}")
	public Produto buscarPorId(@PathVariable String id){
		return produtoService.buscar(id);
	}
	
	@ResponseStatus(NO_CONTENT)
	@DeleteMapping("/{id}")
	public void remover(@PathVariable String id){
		produtoService.remover(id);
	}
	
	@ResponseStatus(NO_CONTENT)
	@PutMapping("/{id}")
	public void alterar(@PathVariable String id, @RequestBody ProdutoIn produto){
		produtoService.alterar(produto, id);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity criar(@RequestBody ProdutoIn produto, ServletUriComponentsBuilder uriBuilder){
		String created = produtoService.criar(produto).getId();
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(created).toUri();
		return ResponseEntity.created(uri).build();
	}

}
