package br.com.sicredi.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.sicredi.interceptors.ConsumerRequestFilter;
import br.com.sicredi.service.RegistroRequestsService;

@Configuration
public class ConsumerConfig {

	RegistroRequestsService service;
	
	public ConsumerConfig(RegistroRequestsService service) {
		this.service = service;
	}
	
	@Bean
	public FilterRegistrationBean<ConsumerRequestFilter> requestFilter(){
	    FilterRegistrationBean<ConsumerRequestFilter> registrationBean 
	      = new FilterRegistrationBean<>();
	         
	    registrationBean.setFilter(new ConsumerRequestFilter(service));
	    List<String> patterns = new ArrayList<String>();
	    patterns.add("/api/produtos/*");
	    patterns.add("/api/*");
	    registrationBean.setUrlPatterns(patterns);
	         
	    return registrationBean;    
	}
	
}
