package br.com.sicredi.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.sicredi.documents.RegistroRequests;

public interface RegistroRequestsRepository extends MongoRepository<RegistroRequests, ObjectId> {

}
