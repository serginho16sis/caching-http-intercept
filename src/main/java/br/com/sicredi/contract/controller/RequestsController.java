package br.com.sicredi.contract.controller;

import static org.springframework.http.HttpStatus.OK;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.sicredi.documents.RegistroRequests;
import br.com.sicredi.repository.RegistroRequestsRepository;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value="/requests", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@AllArgsConstructor
public class RequestsController {
	
	private RegistroRequestsRepository registroRequestsRepository;
	
	@ResponseStatus(OK)
	@GetMapping
	public List<RegistroRequests> listar(){
		return registroRequestsRepository.findAll();
	}
	
	

}
