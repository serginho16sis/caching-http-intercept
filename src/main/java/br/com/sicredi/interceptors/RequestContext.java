package br.com.sicredi.interceptors;

public class RequestContext {

    private static ThreadLocal<String> request = new ThreadLocal<>();
    
    private static ThreadLocal<String> response = new ThreadLocal<>();

    private RequestContext() {
    }

    public static void setRequest(String requestMessage) {
		request.set(requestMessage);
    }
    
    public static String getRequest() {
		return request.get();
	}
    
    public static void setResponse(String responseMessage) {
    	response.set(responseMessage);
    }
    
    public static String getResponse() {
		return response.get();
	}
}
