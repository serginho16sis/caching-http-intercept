package br.com.sicredi.documents;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Document
@Getter
@Setter
@EqualsAndHashCode
public class Produto implements Serializable{

	private static final long serialVersionUID = 5314771495966824610L;

	@Id
	public String id;
	
	private String nome;
	
	private LocalDate dataCadastro;

}
