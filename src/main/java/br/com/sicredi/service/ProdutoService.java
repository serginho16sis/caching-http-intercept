package br.com.sicredi.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.TimeToLive;
import org.springframework.stereotype.Service;

import br.com.sicredi.contract.model.ProdutoIn;
import br.com.sicredi.documents.Produto;
import br.com.sicredi.exceptions.EntityNotFoundException;
import br.com.sicredi.repository.ProdutoRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ProdutoService {
	
	
	
	private ProdutoRepository produtoRepository;
	
	private ModelMapper modelMapper;
	
	@Cacheable(value="listaProdutos")
	public List<Produto> buscarTodos(){
		return produtoRepository.findAll();
	}
	
	@Caching(put= {@CachePut(value = "produto", key="T(br.com.sicredi.documents.Produto)")},
		     evict= @CacheEvict(value = "listaProdutos", allEntries=true))
	public Produto alterar(ProdutoIn produto, String id) {
		Produto altered = modelMapper.map(produto, Produto.class);
		altered.setId(buscar(id).getId());
		return produtoRepository.save(altered);
	}
	
	@Caching(put= {@CachePut(value = "produto", key="T(br.com.sicredi.documents.Produto)")},
		     evict= @CacheEvict(value = "listaProdutos", allEntries=true))
	public Produto criar(ProdutoIn produto) {
		return produtoRepository.save(modelMapper.map(produto, Produto.class));
	}
	
	@Cacheable(value = "produto", key="T(br.com.sicredi.documents.Produto)")
	public Produto buscar(String id) {
		return produtoRepository.findById(id)
								.orElseThrow(() -> new EntityNotFoundException("Produto não encontrado!"));
	}
	
	
	@CacheEvict(value = "listaProdutos", allEntries = true)
	public void remover(String id) {
		produtoRepository.deleteById(id);
	}
}
