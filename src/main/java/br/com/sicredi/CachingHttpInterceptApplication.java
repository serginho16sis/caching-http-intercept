package br.com.sicredi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CachingHttpInterceptApplication {

	public static void main(String[] args) {
		SpringApplication.run(CachingHttpInterceptApplication.class, args);
	}

}
