package br.com.sicredi.contract.model;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoIn {
	private String nome;
	
	private LocalDate dataCadastro;
}
