package br.com.sicredi.service;

import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import br.com.sicredi.documents.RegistroRequests;
import br.com.sicredi.repository.RegistroRequestsRepository;

@Service
@Validated
public class RegistroRequestsService extends DefaultConversionService {

	private final String ZONE_ID;

	private RegistroRequestsRepository repository;

	public RegistroRequestsService(@Value("${consumer.zoneid:America/Sao_Paulo}") String zONE_ID, RegistroRequestsRepository repository) {
		super();
		ZONE_ID = zONE_ID;
		this.repository = repository;

	}

	public void salvarRequest(@NotNull String event, String response) {
		RegistroRequests registroErros = new RegistroRequests(event, response);
		registroErros.setDataLog(LocalDateTime.now(ZoneId.of(ZONE_ID)));
		repository.insert(registroErros);
	}

}