package br.com.sicredi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.sicredi.interceptors.RequestLoggingFilter;

@Configuration
public class RequestLoggingFilterConfiguration {
	
    @Bean
    public RequestLoggingFilter logFilter() {
    	RequestLoggingFilter filter = new RequestLoggingFilter();
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(true);
        filter.setMaxPayloadLength(10000);
        filter.setIncludeHeaders(true);
        return filter;
    }
}