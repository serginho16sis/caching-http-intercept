package br.com.sicredi.documents;

import java.time.LocalDateTime;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@Document(collection = "registroRequests")
@RequiredArgsConstructor
@NoArgsConstructor
public class RegistroRequests {

    @Id
    private ObjectId id;
    @NonNull
    private String request;
    @NonNull
    private String response;
    private LocalDateTime dataLog;
    
}
