package br.com.sicredi.interceptors;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import br.com.sicredi.service.RegistroRequestsService;

public class ConsumerRequestFilter implements Filter {

	private RegistroRequestsService service;
	
	public ConsumerRequestFilter(RegistroRequestsService service) {
		this.service = service;
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		 if (response.getCharacterEncoding() == null) {
	            response.setCharacterEncoding("UTF-8");
	        }

	        HttpServletResponseCopier responseCopier = new HttpServletResponseCopier((HttpServletResponse) response);

	        try {
	            chain.doFilter(request, responseCopier);
	            responseCopier.flushBuffer();
	        } finally {
	            byte[] copy = responseCopier.getCopy();
	            String requestContent = String.format("%s %s %s %s %s", 
	            									  ((HttpServletRequest)request).getMethod(),
	            									  ((HttpServletRequest)request).getRequestURI(),
	            									  request.getReader().lines().collect(Collectors.joining(System.lineSeparator())),
	            									  Collections.list(((HttpServletRequest)request).getHeaderNames())
													             .stream()
													             .collect(Collectors.toMap(
													            	        Function.identity(), 
													            	        h -> Collections.list(((HttpServletRequest)request).getHeaders(h))
													            	    )),
									            	    Collections.list(((HttpServletRequest)request).getParameterNames())
													               .stream()
													               .collect(Collectors.toMap(h -> h, ((HttpServletRequest)request)::getParameter))
													  
													  );
	            String responseContent = String.format("%s %s", new String(copy, response.getCharacterEncoding()), ((HttpServletResponse)response).getStatus() );
	           service.salvarRequest(requestContent, responseContent);
	        }
		
	
	}


}

class HttpServletResponseCopier extends HttpServletResponseWrapper {

    private ServletOutputStream outputStream;
    private PrintWriter writer;
    private ServletOutputStreamCopier copier;

    public HttpServletResponseCopier(HttpServletResponse response) throws IOException {
        super(response);
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        if (writer != null) {
            throw new IllegalStateException("getWriter() has already been called on this response.");
        }

        if (outputStream == null) {
            outputStream = getResponse().getOutputStream();
            copier = new ServletOutputStreamCopier(outputStream);
        }

        return copier;
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        if (outputStream != null) {
            throw new IllegalStateException("getOutputStream() has already been called on this response.");
        }

        if (writer == null) {
            copier = new ServletOutputStreamCopier(getResponse().getOutputStream());
            writer = new PrintWriter(new OutputStreamWriter(copier, getResponse().getCharacterEncoding()), true);
        }

        return writer;
    }

    @Override
    public void flushBuffer() throws IOException {
        if (writer != null) {
            writer.flush();
        } else if (outputStream != null) {
            copier.flush();
        }
    }

    public byte[] getCopy() {
        if (copier != null) {
            return copier.getCopy();
        } else {
            return new byte[0];
        }
    }

}

class ServletOutputStreamCopier extends ServletOutputStream {

    private OutputStream outputStream;
    private ByteArrayOutputStream copy;

    public ServletOutputStreamCopier(OutputStream outputStream) {
        this.outputStream = outputStream;
        this.copy = new ByteArrayOutputStream(1024);
    }

    @Override
    public void write(int b) throws IOException {
        outputStream.write(b);
        copy.write(b);
    }

    public byte[] getCopy() {
        return copy.toByteArray();
    }

	@Override
	public boolean isReady() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setWriteListener(WriteListener listener) {
		// TODO Auto-generated method stub
		
	}

}
