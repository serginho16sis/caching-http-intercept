package br.com.sicredi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.sicredi.documents.Produto;

@Repository
public interface ProdutoRepository extends MongoRepository<Produto, String> {

}
